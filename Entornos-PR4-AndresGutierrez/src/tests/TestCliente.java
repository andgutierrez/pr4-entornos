package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestCliente {

	static Cliente cliente;
	static GestorContabilidad gestor;
	
	//Se crea una instancia de GestorContabilidad una vez, antes de todos los test.
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gestor = new GestorContabilidad();
	}

	
	// Me aseguro que antes de cada test solo hay 1 cliente en la listaClientes
	@Before
	public void setUp(){
		   gestor.getListaClientes().clear();
		   cliente = new Cliente("Andres", "18172070T", LocalDate.parse("2018-03-15"));
		   gestor.getListaClientes().add(cliente);
	}
	
	// ****Metodo: buscarCliente****
	//Comprueba que el metodo buscarCliente devuelve null si no existe
	@Test
	public void testBuscarClienteInexistente(){
		Cliente actual = gestor.buscarCliente("17652415A");
		assertNull(actual);
	}

	//Comprueba que el metodo buscarCliente lo busca correctamente
	@Test
	public void testBuscarClienteExiste(){
		Cliente actual = gestor.buscarCliente("18172070T");
		assertSame(cliente, actual);
	}
	
	// ****Metodo: altaCliente****
	//Comprueba que si un cliente no existe se da de alta
	@Test
	public void testAltaClienteSiNoExiste() {
		Cliente nuevoCliente = new Cliente("Pedro", "17624514K", LocalDate.parse("2018-05-05"));
		gestor.altaCliente(nuevoCliente);
		Cliente actual = null;
		for(int i = 0; i < gestor.getListaClientes().size(); i++) {
			if(gestor.getListaClientes().get(i).getDni().equals("17624514K")) {
				actual = gestor.getListaClientes().get(i);
			}
		}
		assertSame(nuevoCliente, actual);
	}
	
	//Comprueba que si un cliente ya existe no se da de alta
	@Test
	public void testAltaClienteSiYaExiste() {
		Cliente nuevoCliente = new Cliente("Andres", "18172070T", LocalDate.parse("2018-03-15"));
		gestor.altaCliente(nuevoCliente);
		Boolean contieneNuevoCliente = gestor.getListaClientes().contains(nuevoCliente);
		assertFalse(contieneNuevoCliente);
	}
	
	// ****Metodo: clienteMasAntiguo****
	//Comprueba que si hay clientes, busca el m�s antiguo correctamente.
	@Test
	public void testClienteMasAntiguoSiHayClientes() {
		Cliente cliente1 = new Cliente("Carlos", "18412472I", LocalDate.parse("2018-03-12"));
		Cliente cliente2 = new Cliente("Manuel", "17263245Y", LocalDate.parse("2018-01-13"));
		gestor.getListaClientes().add(cliente);
		gestor.getListaClientes().add(cliente1);
		gestor.getListaClientes().add(cliente2);
		Cliente actual = gestor.clienteMasAntiguo();
		assertSame(cliente2, actual);
	}
	
	//Comprueba que si no hay clientes, no devuelve ning�n cliente (devuelve null)
	
	@Test
	public void testClienteMasAntiguoSiNoHayClientes() {
		gestor.getListaClientes().clear();
		Cliente actual = gestor.clienteMasAntiguo();
		assertNull(actual);
	}
	
	// ****Metodo: eliminarCliente****
	//Comprueba que no elimina un cliente que tiene factura asignada
	@Test
	public void testEliminarClienteConFactura() {
		Factura factura = new Factura("0001", LocalDate.parse("2017-10-22"), "Reposteria", 45, 3, cliente);
		gestor.getListaFacturas().add(factura);
		gestor.eliminarCliente("18172070T");
		assertTrue(gestor.getListaClientes().contains(cliente));
		
	}
	
	//Comprueba que elimina un cliente si no tiene factura asignada
	@Test
	public void testEliminarClientes() {
		gestor.eliminarCliente("18172070T");
		assertFalse(gestor.getListaClientes().contains(cliente));	
	}
	
}