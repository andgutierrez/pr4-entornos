package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestFactura {

	static GestorContabilidad gestor;
	static Factura factura;

	//Creo el gestor al principio de la clase
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gestor = new GestorContabilidad();
	}

	// Antes de cada test solo 1 factura en listaFacturas y ningun cliente
	@Before
	public void setUp() throws Exception {
		gestor.getListaFacturas().clear();
		gestor.getListaClientes().clear();
		factura = new Factura("0000", LocalDate.now(), "Perfume", 10, 2);
		gestor.getListaFacturas().add(factura);
	}
	
	// ****Metodo: buscarFactura****
	// Compruebo que si busco una factura con codigo inexistente el metodo devuelve null
	@Test
	public void testBuscarFacturaInexistente() {
		Factura actual = gestor.buscarFactura("0004");
		assertNull(actual);
	}

	// Compruebo que al buscar una factura que existe devuelve esa factura
	@Test
	public void testBuscarFacturaExistente() {
		Factura actual = gestor.buscarFactura("0000");
		assertSame(factura, actual);
	}

	// ****Metodo: crearFactura****
	// Compruebo que crea una factura que no existe
	@Test
	public void testCrearFactura() {
		Factura nuevaFactura = new Factura("0002", LocalDate.parse("2018-01-28"), "Reposteria", 30, 3);
		gestor.crearFactura(nuevaFactura);
		Boolean facturaCreada = gestor.getListaFacturas().contains(nuevaFactura);
		assertTrue(facturaCreada);
	}

	//Comprueba que no se crea una factura si ya existe
	@Test
	public void testCrearFacturaSiExiste() {
		Factura nuevaFactura = new Factura("0000", LocalDate.now(), "Perfume", 10, 2);
		gestor.crearFactura(nuevaFactura);
		Boolean facturaCreada = gestor.getListaFacturas().contains(nuevaFactura);
		assertFalse(facturaCreada);
	}

	// ****Metodo: facturaMasCara****
	// Comprueba cu�l es la factura m�s cara entre 3 facturas
	@Test
	public void testFacturaMasCara() {
		Factura factura1 = new Factura("0002", LocalDate.parse("2018-05-12"), "Perfume", 10, 5);
		Factura factura2 = new Factura("0003", LocalDate.parse("2018-05-12"), "Perfume", 10, 1);
		gestor.getListaFacturas().add(factura1);
		gestor.getListaFacturas().add(factura2);
		Factura actual = gestor.facturaMasCara();
		assertEquals(factura1, actual);
	}
	
	//Comprueba que si no hay facturas, el metodo FacturaMasCara devuelve null
	@Test
	public void testFacturaMasCaraSinFacturas() {
		gestor.getListaFacturas().clear();
		Factura actual = gestor.facturaMasCara();
		assertNull(actual);
	}

	// ****Metodo: facturacionAnual****
	// Comprueba que la facturaci�n anual es correcta al pasarle un a�o que existe
	@Test
	public void testCalcularFacturacionAnual() {
		Factura factura1 = new Factura("0002", LocalDate.parse("2018-05-12"), "Perfume", 10, 5);
		Factura factura2 = new Factura("0003", LocalDate.parse("2018-05-12"), "Perfume", 10, 1);
		gestor.getListaFacturas().add(factura1);
		gestor.getListaFacturas().add(factura2);
		float facturacionAnual = gestor.calcularFacturacionAnual(2018);
		assertEquals(80, facturacionAnual, 0);
	}

	// Comprueba que la facturaci�n anual es 0 si se pasa un a�o que no existe en las facturas
	@Test
	public void testCalcularFacturacionAnualSiNoAnyo() {
		float facturacionAnual = gestor.calcularFacturacionAnual(2017);
		assertEquals(0, facturacionAnual, 0);
	}

	// ****Metodo: eliminarFactura****
	@Test
	public void eliminarFacturaSiExiste() {
		gestor.eliminarFactura("0000");
		assertFalse(gestor.getListaFacturas().contains(factura));

	}
	// ****Metodo: asignarClienteAFactura****
	// Comprueba si se asigna correctamente un cliente que existe a una factura que existe
	@Test
	public void asignarClienteAFactura() {
		Cliente cliente = new Cliente("Andres", "18172070T", LocalDate.parse("2018-03-15"));
		gestor.getListaClientes().add(cliente);
		gestor.asignarClienteAFactura("18172070T", "0000");
		Cliente actual = factura.getCliente();
		assertEquals(cliente, actual);
		
	}
	
	//Comprueba QUE no se asigna un cliente cuando el codigo de la factura pasado no existe
	@Test
	public void asignarClienteAFacturaQueNoExiste() {
		Cliente cliente1 = new Cliente("Manuel", "17623232Y", LocalDate.parse("2018-05-11"));
		gestor.getListaClientes().add(cliente1);
		gestor.asignarClienteAFactura("17623232Y", "009");
		double cantidadFacturas = 0;
		for(int i=0; i< gestor.getListaFacturas().size(); i++) {
			if(gestor.getListaFacturas().get(i).getCliente() != null) {
				if(gestor.getListaFacturas().get(i).getCliente().equals(cliente1)) {
				cantidadFacturas++;
				}
			}
		}
		assertEquals(0,cantidadFacturas,0);
	}
	//Comprueba si no se asigna un cliente a una factura cuando el dni de cliente pasado no existe
	@Test
	public void asignarClienteQueNoExisteAFactura() {
		gestor.asignarClienteAFactura("72996225k", "0000");
		Cliente actual = factura.getCliente();
		assertNull(actual);
	}
	
	// ****Metodo: cantidadFacturasPorCliente****
	//Comprueba que el calculo cantidad de facturas por cliente se realiza correctamente si el cliente existe y tiene facturas
	@Test
	public void cantidadFacturasPorCliente() {
		Cliente cliente = new Cliente("Andres", "18172070T", LocalDate.parse("2018-03-15"));
		gestor.getListaClientes().add(cliente);
		Factura facturaNueva = new Factura("0003", LocalDate.parse("2018-04-18"), "Raton",20, 1, cliente);
		gestor.getListaFacturas().add(facturaNueva);
		double actual = gestor.cantidadFacturasPorCliente("18172070T");
		assertEquals(1, actual,0);	
	}
	
	//Comprueba que la cantidad de facturas por cliente es 0 si no est� asignado a ninguna factura
	@Test
	public void cantidadFacturasPorClienteSinFacturas() {
		Cliente cliente = new Cliente("Andres", "18172070T", LocalDate.parse("2018-03-15"));
		gestor.getListaClientes().add(cliente);
		double actual = gestor.cantidadFacturasPorCliente("18172070T");
		assertEquals(0, actual,0);
	}
}