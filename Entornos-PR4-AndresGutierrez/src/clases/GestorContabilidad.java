package clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Factura> listaFacturas;

	public GestorContabilidad() {
		listaClientes = new ArrayList<Cliente>();
		listaFacturas = new ArrayList<Factura>();
	}
	
	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}
	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}
	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}
	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}
	
	public Cliente buscarCliente(String dni) {
		for (int i = 0; i < listaClientes.size(); i++) {
			if (listaClientes.get(i).getDni().equals(dni)) {
				return listaClientes.get(i);
			}
		}
		return null;
	}

	public Factura buscarFactura(String codigo) {
		for (int i = 0; i < listaFacturas.size(); i++) {
			if (listaFacturas.get(i).getCodigoFactura().equalsIgnoreCase(codigo)) {
				return listaFacturas.get(i);
			}
		}
		return null;
	}

	public void altaCliente(Cliente cliente) {
		if (buscarCliente(cliente.getDni()) == null) {
			listaClientes.add(cliente);
		}

	}

	public void crearFactura(Factura factura) {
		if (buscarFactura(factura.getCodigoFactura()) == null) {
			listaFacturas.add(factura);
		}
	}

	public Cliente clienteMasAntiguo() {
		if (listaClientes.size() > 0) {
			Cliente clienteMasAntiguo = null;
			LocalDate fechaMasAntigua = LocalDate.parse("9999-12-31");
			for (int i = 0; i < listaClientes.size(); i++) {
				if (listaClientes.get(i).getFechaAlta().compareTo(fechaMasAntigua) < 0) {
					clienteMasAntiguo = listaClientes.get(i);
					fechaMasAntigua = listaClientes.get(i).getFechaAlta();
				}
			}
			return clienteMasAntiguo;
		}
		return null;
	}
	
	public Factura facturaMasCara() {
		if(listaFacturas.size() > 0) {
			Factura facturaMasCara = null;
			float precioMasCaro = 0;
			for(int i = 0; i < listaFacturas.size(); i++) {
				if(listaFacturas.get(i).calcularPrecioTotal() > precioMasCaro) {
					facturaMasCara = listaFacturas.get(i);
					precioMasCaro = listaFacturas.get(i).calcularPrecioTotal();
				}
			}
			return facturaMasCara;
		}
		return null;
	}
	
	public float calcularFacturacionAnual(int anyo) {
		float facturacionAnual = 0;
		for(int i = 0; i < listaFacturas.size(); i++) {
			if(listaFacturas.get(i).getFecha().getYear() == anyo) {
				facturacionAnual += listaFacturas.get(i).calcularPrecioTotal();
			}
		}
		return facturacionAnual;
	}
	
	public void asignarClienteAFactura(String dni, String codigoFactura) {
		if((buscarFactura(codigoFactura) != null) && (buscarCliente(dni) != null)) {
			buscarFactura(codigoFactura).setCliente(buscarCliente(dni));
		}
	}
	
	public int cantidadFacturasPorCliente(String dni) {
		int cantidadFacturas = 0;
		for(int i=0; i<listaFacturas.size(); i++) {
			if(listaFacturas.get(i).getCliente() != null) {
				if(listaFacturas.get(i).getCliente().getDni().equals(dni)) {
					cantidadFacturas++;
				}
			}	
		}
		return cantidadFacturas;
	}
	
	public void eliminarFactura(String codigo) {
		listaFacturas.remove(buscarFactura(codigo));
	}
	
	public void eliminarCliente(String dni) {
		if(cantidadFacturasPorCliente(dni) == 0) {
			listaClientes.remove(buscarCliente(dni));
		}
	}
}